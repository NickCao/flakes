## Self
Nick Cao\
Golang / Rust / Nix{,OS}\
An imperfect perfectionist
## Identity
GitLab: [gitlab.com/NickCao](https://gitlab.com/NickCao)\
Telegram: [t.me/NickCao](https://t.me/NickCao)\
Mail: [nickcao@nichi.co](mailto:nickcao@nichi.co)\
GPG: [A1E513A77CC0D91C8806A4EB068A56CEF48FA2C1](https://keys.openpgp.org/vks/v1/by-fingerprint/A1E513A77CC0D91C8806A4EB068A56CEF48FA2C1)
## AS209297
An independently operated research network\
[PeeringDB](https://as209297.peeringdb.com/) [RIPEstat](https://stat.ripe.net/AS209297)
